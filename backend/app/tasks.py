import datetime
import logging
import time
from typing import IO, Any, Dict, Optional

import httpx
import kombu
from celery import Task
from celery._state import get_current_task
from celery.app.log import TaskFormatter
from celery.signals import after_setup_task_logger
from celery.utils.log import get_logger, get_task_logger

from app.main import celery_app

logger = logging.getLogger(__name__)
celery_logger = get_logger(__name__)
task_logger = get_task_logger(__name__)


@after_setup_task_logger.connect
def setup_task_logger(logger: logging.Logger, *args: Any, **kwargs: Any) -> None:
    # logger here is the celery.task logger
    logger.info(logger)
    logger.info(type(logger))
    task_log_handler: TaskLogHandler = TaskLogHandler(celery_app.conf.broker_url)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(task_log_handler)
    logger.info(logger.handlers)
    for handler in logger.handlers:
        handler.setFormatter(
            TaskFormatter(
                "%(asctime)s - %(task_id)s - %(task_name)s - %(name)s - %(levelname)s - %(message)s"
            )
        )


class TaskLogHandler(logging.StreamHandler):
    def __init__(self, brokerurl: Optional[IO[str]]) -> None:
        super(TaskLogHandler, self).__init__(brokerurl)
        self.connection: kombu.Connection = kombu.Connection(brokerurl)
        self.exchange: kombu.Exchange = kombu.Exchange(
            name="task_log", type="topic", durable=True
        )
        self.producer: kombu.Producer = self.connection.Producer(
            self.connection, exchange=self.exchange
        )

    def emit(self, record: logging.LogRecord) -> None:
        task: Task = get_current_task()
        if not (task and task.request):
            return
        message = self.format(record).replace("\00", "")
        timestamp = datetime.datetime.fromtimestamp(record.created).isoformat()
        body = {
            "class": "task_log",
            "type": "log",
            "name": task.name,
            "timestamp": timestamp,
            "level": record.levelno,
            "message": message,
        }
        self.publish_msg(task.request.id, body)

    def publish_msg(self, task_id: str, body: Dict[str, Any]) -> None:
        routing_key: str = (
            task_id if "level" not in body else "{}.{}".format(task_id, body["level"])
        )
        self.producer.publish(
            body,
            routing_key=routing_key,
            retry=True,
            exchange=self.exchange,
            declare=[self.exchange],
        )


@celery_app.task(acks_late=True)
def celery1(word: str) -> str:
    logger.info("celery1")
    return f"celery1 task return {word}"


@celery_app.task(bind=True)
def longtask(self: Task) -> None:
    time.sleep(5)
    logger.info(f"In longtask")
    url = "https://ghibliapi.herokuapp.com/films"
    response = httpx.get(url)
    films = response.json()
    for i, film in enumerate(films):
        meta = {"done": i, "total": len(films) - 1}
        self.update_state(state="PROGRESS", meta=meta)
        logger.info(meta)
        logger.info(f"message: {film.get('title')}")
        time.sleep(1)
