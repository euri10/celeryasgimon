from celery import Celery

from app.settings import get_broker_settings

broker_settings = get_broker_settings()

celery_app = Celery(
    broker=f"amqp://{broker_settings.rabbitmq_default_user}:{broker_settings.rabbitmq_default_pass}@{broker_settings.rabbitmq_hostname}:{broker_settings.rabbitmq_port}"
)
