from datetime import datetime
from enum import Enum
from typing import List, Optional, Union

from pydantic import UUID4, BaseModel


class CeleryMsg(BaseModel):
    msg: str


class CeleryHeartBeat(BaseModel):
    hostname: str
    utcoffset: int
    pid: int
    clock: int
    freq: float
    active: int
    processed: int
    loadavg: List[float]
    sw_ident: str
    sw_ver: str
    sw_sys: str
    timestamp: datetime
    type: str = "worker-heartbeat"


class CeleryEventEnum(str, Enum):
    task_succeeded = "task-succeeded"
    task_sent = "task-sent"
    task_received = "task-received"
    task_started = "task-started"
    task_failed = "task-failed"
    task_rejected = "task-rejected"
    task_revoked = "task-revoked"
    task_retried = "task-retried"


class CeleryTaskBase(BaseModel):
    hostname: str
    utcoffset: int
    pid: int
    clock: int
    uuid: UUID4
    timestamp: Optional[float]
    type: CeleryEventEnum
    local_received: Optional[float]


class CeleryTaskReceived(CeleryTaskBase):
    name: str
    args: str
    kwargs: str
    root_id: UUID4
    parent_id: Optional[UUID4]
    retries: int
    eta: Optional[float]
    expires: Optional[float]


class CeleryTaskSent(CeleryTaskReceived):
    queue: str
    exchange: str
    routing_key: str


class CeleryTaskFailed(CeleryTaskBase):
    exception: str
    traceback: str


class CeleryTaskStarted(CeleryTaskBase):
    pass


class CeleryTaskSucceeded(CeleryTaskBase):
    result: Optional[str]
    runtime: float


class CeleryTaskRejected(CeleryTaskBase):
    pass


class CeleryTaskRevoked(CeleryTaskBase):
    pass


class CeleryTaskRetried(CeleryTaskBase):
    pass


OneOfCeleryTask = Union[
    CeleryTaskFailed,
    CeleryTaskReceived,
    CeleryTaskRejected,
    CeleryTaskRetried,
    CeleryTaskRevoked,
    CeleryTaskSent,
    CeleryTaskStarted,
    CeleryTaskSucceeded,
]
