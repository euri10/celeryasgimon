import uuid

import pytest
from _pytest.logging import LogCaptureFixture
from async_asgi_testclient import TestClient
from asynctest import CoroutineMock, call, patch


@pytest.mark.asyncio
async def test_background(asgiclient: TestClient) -> None:
    async with asgiclient:
        email = uuid.uuid4()
        resp = await asgiclient.post(f"/background/{email}")
        message = "some notification"
        assert resp.json() == {"message": "Notification sent in the background"}
        with open("/tmp/log.txt", mode="r") as email_file:
            assert email_file.read() == f"notification for {email}: {message}"


@pytest.mark.asyncio
async def test_background_async(
    asgiclient: TestClient, caplog: LogCaptureFixture
) -> None:
    # test async background, waiting 1s, getting logs
    async with asgiclient:
        amount = 1
        resp = await asgiclient.post(f"/backgroundasync", json={"amount": amount})
        assert resp.status_code == 200
        assert resp.json() == {"message": f"sleeping {amount} in the back"}
        # assert "sleeping" in caplog.messages
        # assert "slept" in caplog.messages
    # test mocked background, no wait, no log either :)


@pytest.mark.asyncio
async def test_background_async_mock(
    asgiclient: TestClient, caplog: LogCaptureFixture
) -> None:
    async with asgiclient:
        with patch("app.main.background_async", new=CoroutineMock()) as mocked_bga:
            repeat_mock = 1
            for i in range(repeat_mock):
                amount = 100
                resp = await asgiclient.post(
                    f"/backgroundasync", json={"amount": amount}
                )
                assert resp.status_code == 200
                assert resp.json() == {"message": f"sleeping {amount} in the back"}
            assert mocked_bga.call_count == repeat_mock
            mocked_bga.assert_has_calls([call(amount) for i in range(repeat_mock)])
