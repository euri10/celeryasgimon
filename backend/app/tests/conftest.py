import logging
from typing import AsyncGenerator, Generator

import pytest
from async_asgi_testclient import TestClient
from fastapi import FastAPI
from httpx import AsyncClient

from app.main import fastapp

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@pytest.fixture(scope="session")
def fastapi_testapp() -> Generator[FastAPI, None, None]:
    yield fastapp


@pytest.fixture
async def asgiclient(fastapi_testapp: FastAPI) -> AsyncGenerator[TestClient, None]:
    yield TestClient(fastapi_testapp)


@pytest.fixture
async def httpxclient(fastapi_testapp: FastAPI) -> AsyncGenerator[AsyncClient, None]:
    yield AsyncClient(app=fastapi_testapp, base_url="https://domain.tld")
