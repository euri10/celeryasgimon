import React, {useState, useCallback, useEffect, Fragment} from 'react';
import useWebSocket from 'react-use-websocket';

import {
    Alert,
    Badge,
    Card,
    CardBlock,
    CardTitle,
    CardText
} from '@bootstrap-styled/v4';
import {HBNotification} from "./HBNotification";

const CONNECTION_STATUS_CONNECTING = 0;
const CONNECTION_STATUS_OPEN = 1;
const CONNECTION_STATUS_CLOSING = 2;
const CONNECTION_STATUS_CLOSED = 3;

export const WebSocketContainerHB = ({socketUrl}) => {
    const [messageHistory, setMessageHistory] = useState([]);
    const [sendMessage, lastMessage, readyState] = useWebSocket(socketUrl);
    const handleClickSendMessage = useCallback(() => sendMessage('Hello'), []);
    useEffect(() => {
        if (lastMessage !== null) {
            console.log(lastMessage);
            setMessageHistory(prev => prev.concat(lastMessage));
        }
    }, [lastMessage]);
    const connectionStatus = {
        [CONNECTION_STATUS_CONNECTING]: {status: 'Connecting', variant: "info"},
        [CONNECTION_STATUS_OPEN]: {status: 'Open', variant: "success"},
        [CONNECTION_STATUS_CLOSING]: {status: 'Closing', variant: "warning"},
        [CONNECTION_STATUS_CLOSED]: {status: 'Closed', variant: "danger"},
    }[readyState];
    return (
        <Fragment>
            <Badge color={connectionStatus.variant}>{connectionStatus.status}<br/>
                {socketUrl}
            </Badge>
            <HBNotification message={lastMessage !== null ? JSON.parse(lastMessage.data) : {active: null}}/>
        </Fragment>
    );
};
 