import React from 'react';

export const HBNotification = ({message}) => {
    // console.log(message);
    return (
        <table>
            <tr><td>active</td><td>{message.active}</td></tr>
            <tr><td>clock</td><td>{message.clock}</td></tr>
            <tr><td>freq</td><td>{message.freq}</td></tr>
            <tr><td>hostname</td><td>{message.hostname}</td></tr>
            <tr><td>loadavg</td><td>{message.loadavg}</td></tr>
            <tr><td>pid</td><td>{message.pid}</td></tr>
            <tr><td>processed</td><td>{message.processed}</td></tr>
            <tr><td>sw_ident</td><td>{message.sw_ident}</td></tr>
            <tr><td>sw_sys</td><td>{message.sw_sys}</td></tr>
            <tr><td>sw_ver</td><td>{message.sw_ver}</td></tr>
            <tr><td>timestamp</td><td>{message.timestamp}</td></tr>
            <tr><td>type</td><td>{message.type}</td></tr>
            <tr><td>utcoffset</td><td>{message.utcoffset}</td></tr>
        </table>
    );
};

