import React from 'react';
import './App.css';
import './example.css';
import {MyFirstGrid} from "./components/MyFirstGrid"

function App() {
    return (
            <MyFirstGrid/>
    );
}

export default App;
